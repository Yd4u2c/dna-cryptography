@extends('profile.header')
@section('content')
<script src="{{ URL::asset('js/nicEdit.js') }}"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<div class="col-lg-12 container">
	<!--div class="col-lg-12 he"></div-->
	<div class="edit col-lg-8">
		<h3 class="text-center">Compose <i class="fa fa-edit"></i></h3>
		@if(Session::has('success'))
		<h3 class="w3-text-green">{{Session::get('success')}}</h3>
		@endif
		<form method="post" enctype="multipart/form-data" action="{{ url('sendmsg') }}">
			{{csrf_field()}}
			<div class="form-group">
				<label>From:</label>
				<input type="text" name="Name" class="form-control" readonly="" value="{{\Auth::user()->name}}">
			</div>
			<div class="form-group">
				<label>Email:</label>
				<input type="text" name="Email" readonly="" class="form-control" value="{{\Auth::user()->email}}">
			</div>
			<div class="form-group">
				<label>To:</label>
				<input type="email" name="receiver" class="form-control" placeholder="Enter reciever Email address">
			</div>
			<div class="form-group">
				<label>Subject:</label>
				<input type="text" name="Subject" class="form-control" placeholder="Enter reciever Email address">
			</div>
			<div class="form-group"  style="background-color: white; color: #000;">
				<label>Message</label>
				<textarea name="area1" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label for="imageInput">Attachment</label>
				<input data-preview="#preview" name="input_img" type="file" id="imageInput">
			</div>
			<div class="form-group text-right">
				<input type="submit" name="submit" value="Send" class="btn btn-danger">
			</div>
		</div>
	</form>
</div>

<style type="text/css">
.he{
	height: 20px;
}

.edit{
	
	background-color: rgba(200, 100, 0, 0.7);
	padding: 30px;
}

</style>
@endsection