<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Session;
use Auth;
use ZipArchive;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    //
	public function compose_message(Request $request)
	{
    	#::::::::::VALIDATION:::::::::::
		$this->validate($request, [
			'Name' => 'required',
			'Email' => 'required',
			'receiver' => 'required',
			'area1' => 'required'
		]);

		//dd($request->all());
		$data = new Message();
		$data->Name = $request->Name;
		$data->sender_email = $request->Email;
		$data->receiver_email = $request->receiver;
		$data->message = $request->area1;
		$data->subject = $request->Subject;
		

		if ($request->hasFile('input_img')) 
		{
			$image = $request->file('input_img');
			$name = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/images');
			$image->move($destinationPath, $name);
			$data->file = $name;
			$data->save();
			Session::flash('success','Message sent successfully');
		//return redirect()->back()->withSuccess('IT WORKS!');
			return view('profile.compose');

		}
		
	}

	public function readMore($id)
	{
		$user = Message::where('id',$id)->first();
		//dd($user);
		return view('profile.read_more',compact('user'));
		
	}


	public function download($file_name) 
	{
		
		$path = public_path('images/');
		$files[] = public_path() . $path . uniqid() . '1543848392.pdf';
		$zipper->make($zip_filename)->add($files);

	//	$file_path = public_path('images/'.$file_name);
	//	return response()->download($file_path);
	}
}





