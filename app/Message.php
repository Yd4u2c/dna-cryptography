<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
   protected $fillable = ['name','sender_email','receiver_email','Message'];
}
